var liObj = document.getElementById('list__block');

for(let i= 1; i<=100; i++){
    var liElem = document.createElement('li');
    var liElemText = document.createTextNode(i);
    
    if(i%3 === 0 && i%5 === 0){
        liElemText = document.createTextNode("Fizz Buzz");
        liObj.appendChild(liElem).append(liElemText);
    }else if(i%3 === 0){
        liElemText = document.createTextNode("Fizz");
        liObj.appendChild(liElem).append(liElemText);
    }else if(i%5 === 0){
        liElemText = document.createTextNode("Buzz");
        liObj.appendChild(liElem).append(liElemText);
    }else {
        liObj.appendChild(liElem).append(liElemText)
    }
}
 